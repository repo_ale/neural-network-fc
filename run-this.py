from neuralnetwork import NeuralNetwork
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import warnings

warnings.simplefilter('ignore')

print "Neural Network starting"

print "loading norm_mnist_train.csv file"

df = pd.read_csv("./norm_mnist_train.csv")
training_set = df.values

df_test = pd.read_csv("./norm_mnist_test.csv", header=None)
test_values = df_test.values

all_percentages = [0.0]


learning_rate = 3.0

print "learning rate " + str(learning_rate)

np.random.seed()

net = NeuralNetwork()
net.add_layer(30,784)

#net.add_layer(20,30)
#net.add_layer(30,20)

net.add_output_layer(10,30)

net.set_learning_rate(learning_rate)

net.initialize_delta_weights()
mini_batch_size = 10
n_epochs = 5


print "-------PRE TRAINING: "
print "accuracy and error: "


correct_predictions, error_sum = net.test_mnist_set(test_values)
print str(correct_predictions/float(len(test_values))*100) + "% accuracy" + " epoch error: " + str(error_sum)

for epoch in range(n_epochs):
    net.mini_batches_training(training_set, mini_batch_size)
    
    print "-------finish epoch training: ", epoch
    print "accuracy and error: "


    correct_predictions, error_sum = net.test_mnist_set(test_values)
    correct_percentage = correct_predictions/float(len(test_values))*100

    print str(correct_percentage) + "% accuracy" + " epoch error: " + str(error_sum)
    
    all_percentages.append(correct_percentage)

plt.plot(range(len(all_percentages)), all_percentages)
plt.title("accuracy progression")
plt.xlabel("epoch")
plt.ylabel("accuracy")
plt.show()


"""

print "target: "
print training_set[1][1]

print "predicted: "
print net.predict(training_set[1][0])

print "error: "
print net.get_error(training_set[1][0], training_set[1][1])

"""
