import pandas as pd
import numpy as np

df = pd.read_csv("./mnist_train.csv")
set_ = df.values

set_ = np.array(set_, dtype="float32")

set_[:, 1:] = set_[:, 1:] / 255.0


