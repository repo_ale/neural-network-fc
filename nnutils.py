import math
import numpy as np
import decimal

def sigmoid(x):

    if(x > 700):
        return 0.999999
    if(x < -700):
        return 0.000001
    else:
        return 1 / (1 + math.exp(-x))

       
    
def deriv_sigmoid(x):
    return (sigmoid(x))*(1 - sigmoid(x))

vectorized_sigmoid = np.vectorize(sigmoid)

vectorized_deriv_sigmoid = np.vectorize(deriv_sigmoid)

