# neural network

### Python fully connected neural network implementation from scratch

this handcrafted neural network `neuralnetwork.py` is being tested with MNIST set, is a set of handwritten numbers and the goal is to make the model recognize it.
How? First showing the image of the numbers with their respective labels and then asking about new number images what is that number.
We keep an eye on accuracy progression.

`pip install -U scikit-learn`   used for OneHotEncoder

### to start

`python run-this.py`

Max accuracy reached 86% with learning rate = 3

TODO:
- Implement Dropout
- Fight against Overfitting

Developed by
Alexis Batyk