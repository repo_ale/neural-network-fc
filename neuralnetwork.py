import numpy as np
from nnutils import vectorized_sigmoid, vectorized_deriv_sigmoid
from sklearn.preprocessing import OneHotEncoder
import pandas as pd
import matplotlib.pyplot as plt

class NeuralNetwork:
    def __init__(self):
        self.neurons_layer = []

        self.learning_rate = 0.0
        

    def add_layer(self, layer_neurons, previous_layer_neurons):
        layer_to_add = {
                            "weights": np.clip(0.4 * np.random.randn(layer_neurons,previous_layer_neurons), -0.999, 0.999), 
                            "bias": np.clip(0.4 * np.random.randn(layer_neurons,), -0.999, 0.999),
                            "delta_weights": np.zeros((layer_neurons,previous_layer_neurons)),
                            "delta_bias": np.zeros((layer_neurons,))
                        }
        
        self.neurons_layer.append(layer_to_add)

    def add_output_layer(self, layer_neurons, previous_layer_neurons):
        layer_to_add = {
                            "weights": np.clip(0.4 * np.random.randn(layer_neurons,previous_layer_neurons), -0.999, 0.999), 
                            "bias": np.clip(0.4 * np.random.randn(layer_neurons,), -0.999, 0.999),
                            "delta_weights": np.zeros((layer_neurons,previous_layer_neurons)),
                            "delta_bias": np.zeros((layer_neurons,))
                        }

        self.neurons_layer.append(layer_to_add)

    def initialize_delta_weights(self):
        for index_layer in range(len(self.neurons_layer)):
            layer = self.neurons_layer[index_layer]
            layer["delta_weights"] = np.zeros((layer["weights"].shape))
            layer["delta_bias"] = np.zeros((layer["bias"].shape))


    def set_learning_rate(self, learning_rate):
        self.learning_rate = learning_rate

    def get_error(self, X, Y):
        
        output_layer_values = self.predict(X)

        # error calculation
        error = np.sum( 1.0/2.0 * ( (Y - output_layer_values)**2.0 ) )
        return error

    def train(self, input_, target):
        output_layer = self.neurons_layer[-1]

        input_layer = {"output_values":input_}

        output_layer_values = self.predict(input_)

        # error calculation
        error = np.sum( 1.0/2.0 * ( (target - output_layer_values)**2.0 ) )

        ## deltas 
        delta_net_output_layer = (-1)*( target - output_layer_values ) * vectorized_deriv_sigmoid(output_layer_values)
        delta_w_k = np.dot(delta_net_output_layer.reshape(-1,1), self.neurons_layer[len(self.neurons_layer)-2]["output_values"].reshape(1,-1))
        delta_b_k = delta_net_output_layer

        output_layer["weights"] = output_layer["weights"] - self.learning_rate * delta_w_k
        output_layer["bias"] = output_layer["bias"] - self.learning_rate * delta_b_k

        last_calculated_delta_layer = []

        for layer_index in reversed(range(len(self.neurons_layer)-1)):
            layer = self.neurons_layer[layer_index]

            
            if(len(last_calculated_delta_layer) < 1):
                last_calculated_delta_layer = delta_net_output_layer
            

            delta_net = np.dot(last_calculated_delta_layer, self.neurons_layer[layer_index+1]["weights"]) * vectorized_deriv_sigmoid(layer["output_values"])

            delta_weights = []

            if(layer_index == 0):
                delta_weights = np.dot(delta_net.reshape(-1,1), input_layer["output_values"].reshape(1,-1))
            else:  
                delta_weights = np.dot(delta_net.reshape(-1,1), self.neurons_layer[layer_index-1]["output_values"].reshape(1,-1))

            delta_bias = delta_net
        
            layer["weights"] = layer["weights"] - self.learning_rate * delta_weights
            layer["bias"] = layer["bias"] - self.learning_rate * delta_bias

            last_calculated_delta_layer = delta_net


    def get_deltas(self, input_, target):
        output_layer = self.neurons_layer[-1]

        input_layer = {"output_values":input_}

        output_layer_values = self.predict(input_)

        # calcular error
        error = np.sum( 1.0/2.0 * ( (target - output_layer_values)**2.0 ) )

        ## deltas 
        delta_net_output_layer = (-1)*( target - output_layer_values ) * vectorized_deriv_sigmoid(output_layer_values)
        delta_w_k = np.dot(delta_net_output_layer.reshape(-1,1), self.neurons_layer[len(self.neurons_layer)-2]["output_values"].reshape(1,-1))
        delta_b_k = delta_net_output_layer

        output_layer["delta_weights"] += delta_w_k
        output_layer["delta_bias"] += delta_b_k

        last_calculated_delta_layer = []

        for layer_index in reversed(range(len(self.neurons_layer)-1)):
            layer = self.neurons_layer[layer_index]

            
            if(len(last_calculated_delta_layer) < 1):
                last_calculated_delta_layer = delta_net_output_layer
            

            delta_net = np.dot(last_calculated_delta_layer, self.neurons_layer[layer_index+1]["weights"]) * vectorized_deriv_sigmoid(layer["output_values"])

            delta_weights = []

            if(layer_index == 0):
                delta_weights = np.dot(delta_net.reshape(-1,1), input_layer["output_values"].reshape(1,-1))
            else:  
                delta_weights = np.dot(delta_net.reshape(-1,1), self.neurons_layer[layer_index-1]["output_values"].reshape(1,-1))

            delta_bias = delta_net

            layer["delta_weights"] += delta_weights
            layer["delta_bias"] += delta_bias

            last_calculated_delta_layer = delta_net


    def update_weights(self, mini_batch_size):
        for layer_index in range(len(self.neurons_layer)):
            layer = self.neurons_layer[layer_index]

            layer["weights"] = layer["weights"] - (layer["delta_weights"] * self.learning_rate / float(mini_batch_size))
            layer["bias"] = layer["bias"] - (layer["delta_bias"] * self.learning_rate / float(mini_batch_size))


    def predict(self, input_):
        last_calculated_layer = []

        for layer_index in range(len(self.neurons_layer)):
            layer = self.neurons_layer[layer_index]
            
            # ask if is the first layer
            if(len(last_calculated_layer) < 1):
                last_calculated_layer = input_

            feedforward = np.dot(last_calculated_layer, layer["weights"].T) + layer["bias"]

            feedforward = vectorized_sigmoid(feedforward)
            
            layer["output_values"] = feedforward

            # if is the last layer, it will return its values
            # si es la ultima layer, retorna el valor de la capa
            if (layer_index == (len(self.neurons_layer) - 1)):
                return feedforward
            else:
                last_calculated_layer = feedforward

    def get_mini_batch(self, training_set, mini_batch_size):
        mini_batches = [
            training_set[k:k+mini_batch_size]
            for k in xrange(0, len(training_set), mini_batch_size)
        ]
        return mini_batches

    def mini_batches_training(self, training_set, mini_batch_size):
        
        onehot_encoder = OneHotEncoder(sparse=False)
        onehot_encoder.fit(np.arange(10).reshape(-1, 1))

        np.random.shuffle(training_set)

        mini_batches = self.get_mini_batch(training_set, mini_batch_size)

        for index in range(len(mini_batches)):
            self.initialize_delta_weights()

            this_mini_batch = mini_batches[index]
            
            target = onehot_encoder.transform(this_mini_batch[:,0].reshape(-1, 1))
            input_ = this_mini_batch[:,1:]
            
            for training_index in range(len(target)):
                self.get_deltas(input_[training_index], target[training_index])

            self.update_weights(mini_batch_size)

        

    def get_mini_batch_deltas(self, mini_batch, mini_batch_size):
        pass

    def join_training_set(self, target, input_):
        prepared_training_set = [(input_[x], target[x]) for x in range(len(input_))]

        return prepared_training_set
        #norm_target = onehot_encoder.transform(norm.values[:,0].reshape(len(norm.values[:,0]), 1))
        #norm_input = norm.values[:, 1:]
        
    def train_a_set(self, training_set):
        #for index in range(len(training_set)):
        #    input_.append(training_set[index][0])
        #    target.append(training_set[index][1])

        #method -> train(input, target)   
        
        for i in range(len(training_set)):
            self.train(training_set[i][0], training_set[i][1])

    def test_mnist_set(self, test_values):
        
        np.random.shuffle(test_values)

        onehot_encoder = OneHotEncoder(sparse=False)
        onehot_encoder.fit(np.arange(10).reshape(-1, 1))

        counter = 0.0
        error_sum = 0.0

        for i in range(len(test_values)):
            target = onehot_encoder.transform(test_values[:,0].reshape(len(test_values[:,0]), 1))         
            input_ = test_values[:, 1:]

            error_sum += self.get_error(input_[i], target[i])

            if(np.argmax(self.predict(input_[i])) == np.argmax(target[i])):
                counter += 1.0
            
        return counter, error_sum
