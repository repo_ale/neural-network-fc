import numpy as np
from nnutils import vectorized_sigmoid, vectorized_deriv_sigmoid
from sklearn.preprocessing import OneHotEncoder
import pandas as pd
import matplotlib.pyplot as plt
from neuralnetwork import NeuralNetwork
import warnings

warnings.simplefilter('ignore')

df = pd.read_csv("./norm_test_mnist_train.csv")
training_set = df.values

learning_rate = 4.0

np.random.seed()

net = NeuralNetwork()
net.add_layer(30,784)

#net.add_layer(20,30)
#net.add_layer(30,20)

net.add_output_layer(10,30)

net.set_learning_rate(learning_rate)

net.initialize_delta_weights()
mini_batch_size = 10
n_epochs = 5


print "-------PRE TRAINING: "
print "accuracy and error: "

df_test = pd.read_csv("./norm_test_mnist_test.csv", header=None)
test_values = df_test.values

all_percentages = [0.0]

correct_predictions, error_sum = net.test_mnist_set(test_values)
print str(correct_predictions/float(len(test_values))*100) + "% accuracy" + " epoch error: " + str(error_sum)

for epoch in range(n_epochs):
    net.mini_batches_training(training_set, mini_batch_size)
    
    print "-------finish epoch training: ", epoch
    print "accuracy and error: "


    correct_predictions, error_sum = net.test_mnist_set(test_values)
    correct_percentage = correct_predictions/float(len(test_values))*100

    print str(correct_percentage) + "% accuracy" + " epoch error: " + str(error_sum)
    
    all_percentages.append(correct_percentage)

plt.plot(range(len(all_percentages)), all_percentages)
plt.show()


"""

print "target: "
print training_set[1][1]

print "predicted: "
print net.predict(training_set[1][0])

print "error: "
print net.get_error(training_set[1][0], training_set[1][1])

"""
